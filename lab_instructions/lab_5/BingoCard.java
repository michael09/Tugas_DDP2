/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {

	private Number[][] numbers;
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;
		this.numberStates = numberStates;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	public String markNum(int num, String nama) {
		//TODO Implement
		Number angka = numberStates[num];
		if (angka == null) {
			return nama + ": Kartu tidak memiliki angka " + num;
		} else if (angka.isChecked()) {
			return nama + ": " + num + " sebelumnya sudah tersilang";
		} else {
			angka.setChecked(true);
			CheckBingo();
			return nama + ": " + num + " tersilang";
		}
	}
	
	public String info(String nama) {
		//TODO Implement
		StringBuilder hasil = new StringBuilder();
		for (int i = 0; i < 5; i++) {
			hasil.append("|");
			for (int j = 0; j < 5; j++) {
				if (numbers[i][j].isChecked()){
					hasil.append(" X  |");
				} else {
					hasil.append(" ").append(numbers[i][j].getValue()).append(" |");
				}
			}
			if (i < 4) {
				hasil.append("\n");
			}
		}
		return nama + "\n" + hasil;
	}

	public void restart(){
		//TODO Implement
		for (Number n:numberStates) {
			if (n != null) {
				n.setChecked(false);
			}
		}
		System.out.println("Mulligan!");
	}

	public void CheckBingo() {
		boolean horizontal = false;
		boolean vertikal = false;
		boolean diagonal1 = false;
		boolean diagonal2 = false;
		for (int i = 0; i < 5; i++) {
			if (numbers[0][i].isChecked() && numbers[1][i].isChecked() && numbers[2][i].isChecked() && numbers[3][i].isChecked() && numbers[4][i].isChecked()) {
				horizontal = true;
				break;
			}
		}
		for (int i = 0; i < 5; i++) {
			if (numbers[i][0].isChecked() && numbers[i][1].isChecked() && numbers[i][2].isChecked() && numbers[i][3].isChecked() && numbers[i][4].isChecked()) {
				vertikal = true;
				break;
			}
		}
		if (numbers[0][0].isChecked() && numbers[1][1].isChecked() && numbers[2][2].isChecked() && numbers[3][3].isChecked() && numbers[4][4].isChecked()) {
			diagonal1 = true;
		}
		if (numbers[0][4].isChecked() && numbers[1][3].isChecked() && numbers[2][2].isChecked() && numbers[3][1].isChecked() && numbers[4][0].isChecked()) {
			diagonal2 = true;
		}
		if (diagonal1 || diagonal2 || horizontal || vertikal) {
			isBingo = true;
		}
	}
}
