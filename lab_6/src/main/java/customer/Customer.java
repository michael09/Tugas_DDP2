package customer;
import ticket.Ticket;
import theater.Theater;
import movie.Movie;

import java.util.ArrayList;

public class Customer {

    private String nama;
    private int umur;
    private String isPerempuan;
    private ArrayList<Ticket> tiketDibeli = new ArrayList<>();
    private ArrayList<Ticket> tiketDitonton = new ArrayList<>();

    public Customer(String nama, String isPerempuan, int umur) {
        this.nama = nama;
        this.umur = umur;
        this.isPerempuan = isPerempuan;
    }


    public void findMovie(Theater bioskop, String namaFilm) {
        int count = 0;
        for(Movie film : bioskop.getMovie()) {
            if (film.getJudulFilm().equals(namaFilm)) {
                System.out.println("\n------------------------------------------------------------------");
                System.out.println("Judul\t: " + film.getJudulFilm() + "\nGenre\t: " + film.getGenre());
                System.out.println("Durasi\t: " + film.getDurasi() +" menit" + "\nRating\t: " + film.getRating());
                System.out.println("Jenis\t: Film " + film.getJenis());
                System.out.println("\n------------------------------------------------------------------");
                break;
            } else {
                count += 1;
            }
        }
        if (count == bioskop.getMovie().length) {
            System.out.println("Film " + namaFilm + " yang dicari " + this.nama + " tidak ada di bioskop " + bioskop.getNamaTheater());
        }
    }

    public Ticket orderTicket(Theater bioskop, String namaFilm, String hari, String jenis) {
        int count = 0;
        Ticket beli = null;
        for (Ticket tiket : bioskop.getIsiTicket()) {
            String tmpNama = tiket.getFilm().getJudulFilm();
            String tmpHari = tiket.getHari();
            String tmpJenis = tiket.isIs3d();
            if (tmpNama.equals(namaFilm) && tmpHari.equals(hari) && tmpJenis.equals(jenis)) {
                if(this.umur >= tiket.getFilm().MinUmur()) {
                    System.out.println(this.nama + " telah membeli tiket " + tmpNama + " jenis " + tmpJenis + " di " + bioskop.getNamaTheater() + " pada hari " + tiket.getHari() + " seharga Rp. " + tiket.HargaTiket());
                    bioskop.setSaldo(bioskop.getSaldo() + tiket.HargaTiket());
                    beli = tiket;
                    tiketDibeli.add(beli);
                    break;
                } else {
                    System.out.println(this.nama + " masih belum cukup umur untuk menonton " + tiket.getFilm().getJudulFilm() + " dengan rating " + tiket.getFilm().getRating());
                    break;
                }
            } else {
                count+=1;
            }
        }
        if (count == bioskop.getIsiTicket().size()) {
            System.out.println("Tiket untuk film " + namaFilm + " jenis " + jenis + " dengan jadwal " + hari + " tidak tersedia di " + bioskop.getNamaTheater());
        }
        return beli;
    }

    public void watchMovie(Ticket tiket) {
        if (tiketDibeli.contains(tiket)) {
            System.out.println(this.nama + " telah menonton film " + tiket.getFilm().getJudulFilm());
            tiketDitonton.add(tiket);
        } else System.out.println("Tiket belum dibeli");
    }

    public void cancelTicket(Theater bioskop) {
        int panjangTicket = tiketDibeli.size();
        Ticket ticketTerakhir = tiketDibeli.get(panjangTicket - 1);
        if (tiketDitonton.contains(ticketTerakhir)) {
            System.out.println("Tiket tidak bisa dikembalikan karena film " + ticketTerakhir.getFilm().getJudulFilm() + " sudah ditonton oleh " + this.nama);
        } else if (!(bioskop.checkTiket(ticketTerakhir.getFilm()))) {
            System.out.println("Maaf tiket tidak bisa dikembalikan, " + ticketTerakhir.getFilm().getJudulFilm() + " tidak tersedia dalam " + bioskop.getNamaTheater());
        } else if((bioskop.getSaldo() - ticketTerakhir.HargaTiket()) < 0) {
            System.out.println("Maaf ya tiket tidak bisa dibatalkan, uang kas di bioskop " + bioskop.getNamaTheater() + " lagi tekor...");
        } else {
            System.out.println("Tiket film " + ticketTerakhir.getFilm().getJudulFilm() + " dengan waktu tayang " + ticketTerakhir.getHari()
                    +" jenis " + ticketTerakhir.isIs3d() + " dikembalikan ke bioskop " + bioskop.getNamaTheater());
            bioskop.setSaldo(bioskop.getSaldo() - ticketTerakhir.HargaTiket());
            tiketDibeli.remove(ticketTerakhir);
        }
    }
}