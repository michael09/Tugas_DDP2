package ticket;
import movie.Movie;

public class Ticket {

    private Movie film;
    private String hari;
    private boolean is3d;
    private final int HARGA = 60000;

    public Ticket(Movie film, String hari, boolean is3d) {
        this.film = film;
        this.hari = hari;
        this.is3d = is3d;
    }

    public Movie getFilm() {
        return film;
    }

    public String getHari() {
        return hari;
    }

    public String isIs3d() {
        if (is3d) {
            return "3 Dimensi";
        } else {
            return "Biasa";
        }
    }

    public int HargaTiket() {
        int harga = HARGA;
        if (hari.equals("Sabtu") || hari.equals("Minggu")) {
            harga += 40000;
        }
        if (is3d) {
            harga = harga + (int)(harga*0.2);
        }
        return harga ;
    }

}
