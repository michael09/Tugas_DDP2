package theater;
import java.util.ArrayList;
import movie.Movie;
import ticket.Ticket;

public class Theater {

    private String namaTheater;
    private int saldo;
    private ArrayList<Ticket> isiTicket;
    private Movie[] movie;


    public Theater (String namaTheater, int saldo, ArrayList isiTicket, Movie[] movie) {
        this.namaTheater = namaTheater;
        this.saldo = saldo;
        this.isiTicket = isiTicket;
        this.movie = movie;
    }

    public String getNamaTheater() {
        return namaTheater;
    }

    public void setNamaTheater(String namaTheater) {
        this.namaTheater = namaTheater;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public ArrayList<Ticket> getIsiTicket() {
        return isiTicket;
    }

    public void setIsiTicket(ArrayList<Ticket> isiTicket) {
        this.isiTicket = isiTicket;
    }

    public Movie[] getMovie() {
        return movie;
    }

    public void setMovie(Movie[] movie) {
        this.movie = movie;
    }

    public void printInfo() {
        System.out.println("------------------------------------------------------------------");
        System.out.println("Bioskop\t\t\t\t\t: " + namaTheater);
        System.out.println("Saldo Kas\t\t\t\t: " + saldo);
        System.out.println("Jumlah tiket tersedia\t: " + isiTicket.size());
        System.out.print("Daftar Film tersedia\t: ");
        int banyakFilm = movie.length;
        for(int i = 0; i < banyakFilm; i++) {
            if (i == banyakFilm - 1) {
                System.out.print(movie[i].getJudulFilm());
            }
            else {
                System.out.print(movie[i].getJudulFilm() + ", ");
            }
        }
        System.out.println("\n------------------------------------------------------------------");
    }

    public static void printTotalRevenueEarned(Theater[] listTheaters) {
        int jumlahRevenue = 0;
        for(Theater bioskop: listTheaters) {
            jumlahRevenue = jumlahRevenue + bioskop.getSaldo();
        }
        System.out.println("Total uang yang dimiliki Koh Mas : Rp." + jumlahRevenue);
        System.out.println("------------------------------------------------------------------");
        for (Theater bioskop: listTheaters) {
            System.out.println("Bioskop\t\t: " + bioskop.getNamaTheater());
            System.out.println("Saldo Kas\t: Rp." + bioskop.getSaldo() + "\n");
        }
        System.out.println("------------------------------------------------------------------");

    }

    public boolean checkTiket(Movie film) {
        boolean hasil = false;
        for(Movie isi : movie) {
            if (isi.equals(film)) {
                hasil = true;
                break;
            }
        }
        return hasil;
    }

}