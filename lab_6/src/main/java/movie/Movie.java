package movie;
public class Movie {

    private String judulFilm;
    private String rating;
    private int durasi;
    private String genre;
    private String jenis;
    private int minimalUmur;

    public Movie (String judulFilm, String rating, int durasi, String genre, String jenis) {
        this.judulFilm = judulFilm;
        this.rating = rating;
        this.durasi = durasi;
        this.genre = genre;
        this.jenis = jenis;
    }

    public int MinUmur() {
        switch (this.rating) {
            case "Dewasa":
                return 17;
            case "Remaja":
                return 13;
            default:
                return 0;
        }
    }

    public String getJudulFilm() {
        return judulFilm;
    }

    public String getRating() {
        return rating;
    }

    public int getMinimalUmur() {
        return minimalUmur;
    }

    public int getDurasi() {
        return durasi;

    }

    public String getGenre() {
        return genre;
    }

    public String getJenis() {
        return jenis;
    }

    @Override
    public boolean equals(Object obj) {
        return this == (Movie) obj;
    }
}
