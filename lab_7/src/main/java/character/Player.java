package character;

import java.util.ArrayList;

public class Player{
    private static final int ATTACK = 10;
    private String name;
    private int hp;
    private ArrayList<Player> listDimakan = new ArrayList<>();
    private boolean isBurned;
    private String jenis;
    public Player(String name,int hp) {
        this.name = name;
        this.hp = hp;
        this.isBurned = false;
        if (this instanceof Human) {
            this.jenis = "Human";
        } else if (this instanceof Monster) {
            this.jenis = "Monster";
        } else {
            this.jenis = "Magician";
        }
        if (this.hp < 0) {
            this.hp = 0;
        }
    }

    public String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public boolean isBurned() {
        return isBurned;
    }

    public void setBurned(boolean burned) {
        isBurned = burned;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public ArrayList<Player> getListDimakan() {
        return listDimakan;
    }

    public String getJenis() {
        return jenis;
    }

    public boolean canEat(Player dimakan) {
        if (dimakan.getHp() == 0 && dimakan.isBurned && (this instanceof Human || this instanceof Magician) &&
                (dimakan instanceof Monster)) {
            return true;
        } else if (dimakan.getHp() == 0 && this instanceof Monster && !(dimakan instanceof Monster)) {
            return true;
        } else {
            return false;
        }
    }

    public void attack(Player diattack) {
        if (diattack instanceof Magician) {
            diattack.setHp(diattack.getHp() - ATTACK * 2);
        } else {
            diattack.setHp(diattack.getHp() - ATTACK);
        }
        if (diattack.getHp() < 0) {
            diattack.setHp(0);
        }
    }

    public void eat(Player dimakan) {
        this.hp += 15;
        listDimakan.add(dimakan);
    }
}