package character;

public class Magician extends Player{
    public Magician(String nama, int hp) {
        super(nama, hp);
    }

    public void burn(Player dibakar) {
        this.attack(dibakar);
        if (dibakar.getHp() <= 0) {
            dibakar.setBurned(true);
        }
    }
}