package character;

public class Monster extends Player{
    private String roar;

    public Monster(String nama, int hp, String roar) {
        super(nama, hp * 2);
        this.roar = roar;
    }

    public Monster(String nama, int hp) {
        this(nama, hp, "AAAAAAaaaAAAAAaaaAAAAAA");
    }

    public String roar() {
        return roar;
    }
}