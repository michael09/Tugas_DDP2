import character.*;
import java.util.ArrayList;

public class Game{
    ArrayList<Player> player = new ArrayList<Player>();
    ArrayList<Player> termakan = new ArrayList<>();
    /**
     * Fungsi untuk mencari karakter
     * @param "String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name){
        for (Player pemain: player) {
            if (pemain.getName().equals(name)) {
                return pemain;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param "String chara nama karakter yang ingin ditambahkan
     * @param "String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param "int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp){
        if (find(chara) == null) {
            switch (tipe) {
                case "Human":
                    player.add(new Human(chara, hp));
                    break;
                case "Magician":
                    player.add(new Magician(chara, hp));
                    break;
                case "Monster":
                    player.add(new Monster(chara, hp));
            }
            return chara + " ditambah ke game";
        } else {
            return "Sudah ada karakter bernama " + chara;
        }
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param "String chara nama karakter yang ingin ditambahkan
     * @param "String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param "int hp hp dari karakter yang ingin ditambahkan
     * @param "String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar){
        if (find(chara) == null) {
            player.add(new Monster(chara, hp, roar));
            return chara + " ditambah ke game";
        } else {
            return "Sudah ada karakter bernama " + chara;
        }
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param "String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        Player pemain = find(chara);
        if (pemain != null) {
            player.remove(pemain);
            return pemain.getName() + " dihapus dari game";
        } else {
            return "Tidak ada " + chara;
        }
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param "String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public StringBuilder status(String chara){
       Player character = find(chara);
       StringBuilder hasil = new StringBuilder();
       if (character != null) {
           hasil.append(character.getJenis()).append(" ").append(character.getName()).append("\n");
           hasil.append("HP: ").append(character.getHp()).append("\n");
           if (character.getHp() == 0) {
               hasil.append("Sudah meninggal dunia dengan damai\n");
           } else {
               hasil.append("Masih hidup\n");
           }
           hasil.append(diet(chara));
       }
       return hasil;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public StringBuilder status(){
        StringBuilder hasil = new StringBuilder();
        if (player.size() > 0) {
            hasil.append("\n");
            for (Player character : player) {
                hasil.append(status(character.getName()));
            }
        } else {
            hasil.append("Tidak ada pemain");
        }
        return hasil;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param "String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public StringBuilder diet(String chara){
        Player character = find(chara);
        StringBuilder hasil = new StringBuilder();
        if (character != null) {
            if (character.getListDimakan().size() != 0) {
                hasil.append("Memakan ");
                for (Player termakan: character.getListDimakan()) {
                    hasil.append(termakan.getJenis()).append(" ")
                            .append(termakan.getName()).append(", ");
                }
                hasil.delete(hasil.length()-2,hasil.length());
                hasil.append("\n");
            } else {
                hasil.append("Belum memakan siapa siapa\n");
            }
        } else {
            hasil.append("Tidak ada ").append(chara);
        }
        return hasil;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public StringBuilder diet(){
        StringBuilder hasil = new StringBuilder();
        if (termakan.size() > 0) {
            hasil.append("Termakan: ");
            for (Player chara : termakan) {
                hasil.append(chara.getJenis()).append(" ").append(chara.getName()).append(", ");
            }
        } else {
            hasil.append("Belum ada yang termakan");
        }
        return hasil;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param "String meName nama dari character yang sedang dimainkan
     * @param "String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName){
        Player me = find(meName);
        Player enemy = find(enemyName);
        if (cekPemain(me) && cekPemain(enemy)) {
            if (me.getHp() != 0) {
                me.attack(enemy);
                return "Nyawa " + enemy.getName() + " " + enemy.getHp();
            } else {
                return me.getName() + " tidak bisa menyerang " + enemy.getName();
            }
        } else {
            return "Tidak ada " + meName + " atau " + enemyName;
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param "String meName nama dari character yang sedang dimainkan
     * @param "String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName){
        Player me = find(meName);
        Player enemy = find(enemyName);
        String hasil = "";
        if (!cekPemain(me) || !cekPemain(enemy)) {
            return "Tidak ada " + meName + " atau " + enemyName;
        } else if (me instanceof Magician && me.getHp() > 0) {
            ((Magician) me).burn(enemy);
            hasil += "Nyawa " + enemy.getName() + " " + enemy.getHp();
            if (enemy.getHp() == 0) {
                hasil += " \n dan matang";
            }
        } else {
            hasil =  meName + " tidak bisa membakar " + enemyName;
        }
        return hasil;
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
      * * @param "String meName nama dari character yang sedang dimainkan
     * @param "String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName){
        Player me = find(meName);
        Player enemy = find(enemyName);
        if (!cekPemain(me) || !cekPemain(enemy)) {
            return "Tidak ada " + meName + " atau " + enemyName;
        }else if (me.canEat(enemy) && me.getHp() > 0) {
            me.eat(enemy);
            player.remove(enemy);
            termakan.add(enemy);
            return me.getName() + " memakan " + enemyName + "\nNyawa " + me.getName() + " kini " + me.getHp();
        } else {
            return meName + " tidak bisa memakan " + enemyName;
        }
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param "String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName){
        Player me = find(meName);
        if (!cekPemain(me)) {
            return "Tidak ada " + meName;
        } else if (me instanceof Monster && me.getHp() > 0) {
            return ((Monster) me).roar();
        } else {
            return me.getName() + " tidak bisa berteriak";
        }
    }

    public boolean cekPemain(Player me) {
        if (me == null) {
            return false;
        } else {
            return true;
        }
    }

    public void cetakMenu() {
        for (Player orang: player) {
            System.out.println(tree(orang));
        }
    }

    public String tree(Player orang) {
        String hasil = "";
        if (orang.getListDimakan().size() == 0) {
            hasil += orang.getName();
        } else {
            hasil += orang.getName() + " << ( ";
            for (int x = 0; x < orang.getListDimakan().size(); x++) {
                if (x == orang.getListDimakan().size()-1) {
                    hasil += tree(orang.getListDimakan().get(x));
                } else {
                    hasil += tree(orang.getListDimakan().get(x)) + " | ";
                }
            }
            hasil += " )";
        }
        return hasil;
    }
}