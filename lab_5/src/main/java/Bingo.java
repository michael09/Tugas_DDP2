import java.util.Scanner;

public class Bingo {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int jumlahPemain = input.nextInt();
        String[] nama = new String[jumlahPemain];
        Player[] pemain = new Player[jumlahPemain];
        for (int i = 0; i < jumlahPemain; i++) {
            nama[i] = input.next();
        }

        for (int i = 0; i < jumlahPemain; i++) {
            Number[][] numbers = new Number[5][5];
            for (int b = 0; b < 5; b++) {
                for (int a = 0; a < 5; a++) {
                    int masukkan = Integer.parseInt(input.next());
                    numbers[b][a] = new Number(masukkan, b, a);
                }
            }
            Number[] states = new Number[100];
            for (int z = 0; z < 5; z++) {
                for (int j = 0; j < 5; j++) {
                    states[numbers[z][j].getValue()] = numbers[z][j];
                }
            }
            pemain[i] = new Player(new BingoCard(numbers, states),nama[i]);
        }

        boolean gameall = false;
        while (!gameall) {
            String masukkan;
            masukkan = input.next();
            switch (masukkan) {
                case "MARK":
                    for(Player orang : pemain) {
                        if (orang.isWin()) System.out.println("Pemain sudah menang");
                        else {
                            System.out.println("MARK " + orang.getNama() + ": ");
                            int angkaMark = input.nextInt();
                            System.out.println(orang.getKartu().markNum(angkaMark, orang.getNama()));
                            if (orang.getKartu().isBingo()) {
                                System.out.println("BINGO!");
                                System.out.println(orang.getKartu().info(orang.getNama()));
                                orang.setWin(true);
                            }
                        }
                    }
                    break;
                case "INFO":
                    String namaPemain = input.next();
                    for(Player orang : pemain) {
                        if (namaPemain.equals(orang.getNama())) {
                            System.out.println(orang.getKartu().info(orang.getNama()));
                            break;
                        }
                    }
                    break;
                case "RESTART":
                    String namaPemain1 = input.next();
                    for(Player orang : pemain) {
                        if (namaPemain1.equals(orang.getNama())) {
                            if (orang.isRestart()){
                                System.out.println(orang.getNama() + " sudah pernah mengajukan RESTART");
                                break;
                            }
                            orang.setRestart((true));
                            for (Player player : pemain) {
                                player.getKartu().restart();
                                player.setWin(false);
                            }
                        }
                    }
                    break;
                default:
                    System.out.println("Incorrect command");
            }
            gameall = true;
            for (Player orang : pemain) {
                gameall = orang.isWin() && gameall;
            }
        }
        System.out.println("Semua pemain sudah menang");
    }
}
