public class Player {
    private boolean restart;
    private boolean win;
    private BingoCard kartu;
    private String nama;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Player (BingoCard kartu, String nama) {
        this.kartu = kartu;
        this.restart = false;
        this.win = false;
        this.nama = nama;

    }

    public boolean isRestart() {
        return restart;
    }

    public void setRestart(boolean restart) {
        this.restart = restart;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public BingoCard getKartu() {
        return kartu;
    }

    public void setKartu(BingoCard kartu) {
        this.kartu = kartu;
    }
}
