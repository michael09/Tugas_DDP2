import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Michael Wiryadinata Halim, NPM 1706039944, Kelas DDP2 C, GitLab Account: michael09
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		short panjang;
		short lebar;
		short tinggi;
		float berat;
		byte makanan;
		String error = "WARNING: Keluarga ini tidak perlu direlokasi";
		try {
			panjang = input.nextShort();
			if (panjang <= 0 || panjang>= 250) {
				System.out.println(error);
				return;
			}
			System.out.print("Lebar Tubuh (cm)       : ");
			lebar = input.nextShort();
			if (lebar <= 0 || lebar>= 250) {
				System.out.println(error);
				return;
			}
			System.out.print("Tinggi Tubuh (cm)      : ");
			tinggi = input.nextShort();
			if (tinggi <= 0 || tinggi>= 250) {
				System.out.println(error);
				return;
			}
			System.out.print("Berat Tubuh (kg)       : ");
			berat = input.nextFloat();
			if (berat <= 0 || berat >= 150) {
				System.out.println(error);
				return;
			}
			System.out.print("Jumlah Anggota Keluarga: ");
			makanan = input.nextByte();
			if (makanan <= 0 || makanan > 20) {
				System.out.println(error);
				return;
			}
		} catch (Exception e) {
			System.out.println(error);
			return ;
		}
		input.nextLine();
		String tanggalLahir;
		String tahun;
		try {
			System.out.print("Tanggal Lahir          : ");
			tanggalLahir = input.nextLine();
			String[] tanggal = tanggalLahir.split("-");
			for (String w:tanggal){
				Short.parseShort(w);
			}
			tahun = tanggal[2];
			if (Short.parseShort(tahun) >= 2018){
				System.out.println(error);
				return;
			}
		} catch (Exception e) {
			System.out.println(error);
			return ;
		}
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		String hasilCatatan = "Catatan: " + catatan ;
		System.out.print("Jumlah Cetakan Data    : ");
		byte jumlahCetakan;
		try {
			jumlahCetakan = input.nextByte();
		} catch (Exception e) {
			System.out.println(error);
			return;
		}


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		short rasio = (short) ((berat) / (panjang * (float) lebar * tinggi / 1000000));
		input.nextLine();

		for (byte count = 1; count <= jumlahCetakan; count++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " + count + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase

			// TODO Periksa ada catatan atau tidak
			if (catatan.equals("")) hasilCatatan = "Tidak ada catatan tambahan";

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil = "DATA SIAP DICETAK UNTUK " + penerima + "\n-------------------- \n" + nama +
					" - " + alamat + " \nLahir pada tanggal "
					+ tanggalLahir + "\nRasio Berat Per Volume       = " + rasio + " kg/m^3 \n" + hasilCatatan;
			System.out.println(hasil);
		}

		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		int totalAscii = 0;
		for (int ulang = 0; ulang < nama.length(); ulang++){
			char character = nama.charAt(ulang);
			int ascii = (int) character;
			totalAscii = totalAscii + ascii;
		}
		int nomor = ((panjang * tinggi * lebar) + totalAscii) % 10000;



		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		char potong = nama.charAt(0);
		String nomorKeluarga = potong + "" + nomor;

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = 50000 * 365 * makanan;

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		short tahunLahir = Short.parseShort(tahun); // lihat hint jika bingung
		short umur = (short) ((2018) - (tahunLahir));

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String namaApartmen;
		String kabupaten;
		if (umur > 18 && anggaran > 100000000 ){
			namaApartmen = "Mares";
			kabupaten = "Margonda";
		}else if (umur > 18){
			namaApartmen = "Teksas";
			kabupaten = "Sastra";
		}else {
			namaApartmen = "PPMT";
			kabupaten = "Rotunda";
		}

		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "REKOMENDASI APARTEMEN \n-------------------- \nMENGETAHUI: Identitas keluarga: " + nama
				+ " - " + nomorKeluarga + " \nMENIMBANG:  Anggaran" +
				" makanan tahunan: RP." + anggaran + "\n            Umur kepala keluarga: " + umur + " tahun \nMEMUTUSKAN: " +
				"Keluarga " + nama + " akan ditempatkan di : \n" + namaApartmen +", kabupaten " + kabupaten ;
		System.out.println(rekomendasi);

		input.close();
	}
}