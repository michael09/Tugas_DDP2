package Karyawan;

import java.util.ArrayList;

public abstract class Karyawan {

    private static final int BAWAHAN = 10;
    private String nama;
    private String tipe;
    private int dapatGaji = 0;
    private int gaji;
    private ArrayList<Karyawan> listbawahan = new ArrayList<>();
    public Karyawan(String nama, String tipe, int gaji) {
        this.nama = nama;
        this.tipe = tipe;
        this.gaji = gaji;
    }

    public String getNama() {
        return nama;
    }

    public int getGaji() {
        return gaji;
    }

    public String getTipe() {
        return tipe;
    }

    public boolean canAdd(Karyawan bawahan) {
        if (this instanceof Manager) {
            if (!(bawahan instanceof Manager)) {
                return true;
            }
        } else if (this instanceof Staff) {
            if (!(bawahan instanceof Staff || bawahan instanceof Manager)) {
                return true;
            }
        }
        return false;
    }
    public void tambahBawahan(Karyawan bawahan) {
        if (listbawahan.contains(bawahan)) {
            System.out.println("Karyawan " + bawahan.getNama() + " telah menjadi bawahan " + this.nama);
        } else if (listbawahan.size() >= BAWAHAN) {
            System.out.println("Jumlah bawahan Anda sudah melebihi batas maksimal");
        } else if (!canAdd(bawahan)) {
            System.out.println("Anda tidak layak memiliki bawahan");
        } else if (listbawahan.contains(bawahan)) {
            System.out.println("Karyawan " + bawahan.getNama() + " telah menjadi bawahan " + this.getNama());
        }
        else {
            this.listbawahan.add(bawahan);
            System.out.println("Karyawan " + bawahan.getNama() + " berhasil ditambahkan menjadi " +
                    "bawahan " + this.nama);
        }
    }

    public boolean gajian() {
        dapatGaji += 1;
        if (dapatGaji == 6) {
            int gajiSebelum = this.gaji;
            this.gaji += gajiSebelum*0.01;
            System.out.println(this.nama + " mengalami kenaikan gaji sebesar 10% dari "
                    + gajiSebelum + " menjadi " + this.gaji);
            this.dapatGaji = 0;
        }
        if (this instanceof Staff) {
            if (this.gaji > ((Staff) this).getMaks()) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Karyawan> getListbawahan() {
        return listbawahan;
    }

    public void setListbawahan(ArrayList<Karyawan> listbawahan) {
        this.listbawahan = listbawahan;
    }
}