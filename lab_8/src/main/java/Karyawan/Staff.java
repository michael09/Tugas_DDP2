package Karyawan;

public class Staff extends Karyawan {
    private static int maks;
    public Staff (String nama,String tipe, int gaji) {
        super(nama, tipe, gaji);
    }

    public int getMaks() {
        return maks;
    }

    public static void setMaks(int maks) {
        Staff.maks = maks;
    }
}