import Karyawan.Karyawan;
import Karyawan.Intern;
import Karyawan.Manager;
import Karyawan.Staff;

import java.util.ArrayList;

public class Perusahaan {

    private static final int JUMLAH_MAKS = 10000;
    private static final ArrayList<Karyawan> listKaryawan = new ArrayList<>();

    public static void setMaxSalaryStaff(int maxSalaryStaff) {
        Staff.setMaks(maxSalaryStaff);
    }

    private static Karyawan find(String name) {
        for (Karyawan pekerja: listKaryawan) {
            if (pekerja.getNama().equals(name)) {
                return pekerja;
            }
        }
        return null;
    }

    public static void add(String nama, String tipe, int gaji) {
        if (find(nama) != null) {
            System.out.println("Karyawan dengan nama " + nama + " telah terdaftar");
        } else if (listKaryawan.size() > JUMLAH_MAKS) {
            System.out.println("Jumlah karyawan PT TAMPAN sudah penuh..");
         } else {
            boolean flag = false;
            switch (tipe) {
                case "MANAGER" :
                    listKaryawan.add(new Manager(nama, tipe, gaji));
                    break;
                case "STAFF" :
                    listKaryawan.add(new Staff(nama, tipe, gaji));
                    break;
                case "INTERN":
                    listKaryawan.add(new Intern(nama, tipe, gaji));
                    break;
                default:
                    System.out.println("Tidak ada posisi " + tipe);
                    flag = true;
                    break;
            }
            if (flag) {
                System.out.println("Penambahan karyawan gagal");
            } else {
                System.out.println(nama + " mulai bekerja sebagai " + tipe + " di PT TAMPAN");
            }
        }
    }

    public static void tambahBawahan(String nama, String namaBawahan) {
        Karyawan atasan = find(nama);
        Karyawan bawahan = find(namaBawahan);
        if (atasan != null && bawahan != null) {
            atasan.tambahBawahan(bawahan);
        } else {
            System.out.println("Nama tidak berhasil ditemukan");
        }
    }

    public static void gajian() {
        ArrayList<Manager> managerBaru = new ArrayList<>();
        System.out.println("Setiap KARYAWAN yang terdaftar pada KORPORASI akan mendapatkan gaji.");
        for (Karyawan pekerja: listKaryawan) {
            if (pekerja.gajian()) {
                listKaryawan.remove(pekerja);
                Manager baru = new Manager(pekerja.getNama(), "MANAGER", pekerja.getGaji());
                managerBaru.add(baru);
                baru.setListbawahan(pekerja.getListbawahan());
                System.out.print("Selamat, " + pekerja.getNama() + " telah dipromosikan menjadi MANAGER");

            }
        }
        if (managerBaru.size() != 0) {
            listKaryawan.addAll(managerBaru);
        }
    }

    public static void status(String namaPekerja) {
        Karyawan pekerja = find(namaPekerja);
        if (pekerja != null) {
            System.out.println(pekerja.getNama() + " " + pekerja.getGaji());
        } else {
            System.out.println("Nama tidak berhasil ditemukan");
        }
    }
}
