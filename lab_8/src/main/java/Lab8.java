import java.util.Scanner;
public class Lab8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int gajiMaksimum = Integer.parseInt(input.nextLine());
            if (gajiMaksimum > 0) {
                Perusahaan.setMaxSalaryStaff(gajiMaksimum);
                System.out.println("Batas gaji maksimal STAFF: " + gajiMaksimum);
            }
            while (input.hasNextLine()) {
                String[] input_split = input.nextLine().split(" ");
                if (input_split[0].toUpperCase().equals("TAMBAH_KARYAWAN")) {
                    Perusahaan.add(input_split[1].toUpperCase(), input_split[2], Integer.parseInt(input_split[3]));
                } else if (input_split[0].toUpperCase().equals("STATUS")) {
                    Perusahaan.status(input_split[1]);
                } else if (input_split[0].toUpperCase().equals("GAJIAN")) {
                    Perusahaan.gajian();
                } else if (input_split[0].toUpperCase().equals("TAMBAH_BAWAHAN")) {
                    Perusahaan.tambahBawahan(input_split[1], input_split[2]);
                } else {
                    System.out.println("FORMAT MASUKAN SALAH!");
                }
            }
        } catch (Exception e) {
            System.out.println("FORMAT MASUKAN SALAH!");
        }
    }

}