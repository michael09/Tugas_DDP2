import java.util.ArrayList;

public class Manusia {

    private String nama;
    private int umur;
    private int uang;
    private float kebahagiaan ;
    private static ArrayList<Manusia> listObject = new ArrayList<>();



    public Manusia(String nama,int umur,int uang){
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        this.kebahagiaan = 50;
        listObject.add(this);
    }

    public Manusia(String nama,int umur){
        this(nama,umur,50000);
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(byte umur) {
        this.umur = umur;
    }

    public int getUang() {
        return uang;
    }

    public void setUang(int uang) {
        this.uang = uang;
    }

    public float getKebahagiaan() {
        return kebahagiaan;
    }

    public void setKebahagiaan(float kebahagiaan) {
        this.kebahagiaan = kebahagiaan;
    }


    public void beriUang(Manusia penerima,int jumlahUang){
        if ((!(listObject.contains(this)))) {
            System.out.println("Maaf " + nama + " telah meninggal dengan tenang");
            return;
        }
        if (!(listObject.contains(penerima))){
            System.out.println("Maaf " + penerima.getNama() + " telah meninggal dengan tenang");
            return;
        }
        if (jumlahUang <= uang ){
            float tambahBahagia = (float)(jumlahUang / 6000.0);
            kebahagiaan += tambahBahagia;
            penerima.setKebahagiaan(penerima.getKebahagiaan() + tambahBahagia);
            uang = uang - jumlahUang;
            penerima.setUang(penerima.getUang() + jumlahUang);
            if (kebahagiaan > 100){
                kebahagiaan = 100;
            }
            if (penerima.getKebahagiaan() > 100){
                penerima.setKebahagiaan(100);
            }
            System.out.println(nama + " memberi uang sebanyak " + jumlahUang + " kepada " + penerima.getNama() + ", mereka berdua senang :D");
        }
        else {
            System.out.println(nama + " ingin memberi uang kepada " + penerima.getNama() + " namum tidak memiliki cukup uang :'(");
        }
    }

    public void beriUang(Manusia penerima){
        int jumlah = 0;
        for (int i = 0; i < penerima.getNama().length();i++){
            int ascii = (int) penerima.getNama().charAt(i);
            jumlah += ascii;
        }
        int jumlahUang = jumlah * 100;
        this.beriUang(penerima,jumlahUang);
    }

    public void bekerja(int durasi, int bebanKerja){
        if (!(listObject.contains(this))){
            System.out.println(nama + "  telah tiada");
            return;
        }
        if (umur >= 18){
            int bebanKerjaTotal = durasi * bebanKerja;
            if (bebanKerjaTotal <= kebahagiaan){
                kebahagiaan -= bebanKerjaTotal;
                int pendapatan = bebanKerjaTotal * 10000;
                uang += pendapatan;
                System.out.println(nama + " bekerja full time, total pendapatan : " + pendapatan);
            }
            else {
                int durasiBaru = (int)(kebahagiaan/bebanKerja);
                bebanKerjaTotal = durasiBaru * bebanKerja;
                int pendapatan = bebanKerjaTotal * 10000;
                uang += pendapatan;
                kebahagiaan -= bebanKerjaTotal;
                System.out.println(nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);
            }
        }
        else {
            System.out.println(nama + " belum boleh bekerja karena masih dibawah umur D:");
        }
    }

    public void rekreasi(String namaTempat){
        if (!(listObject.contains(this))){
            System.out.println(nama + "  telah tiada");
            return;
        }
        int panjangNama = namaTempat.length();
        int biaya = panjangNama * 10000;
        if (biaya <= uang){
            uang -= biaya;
            kebahagiaan += panjangNama;
            System.out.println(nama + " berekreasi di " + namaTempat + ", " + nama + " senang :)");
        }
        else {
            System.out.println(nama + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat +" :(");
        }
        if (kebahagiaan > 100){
            kebahagiaan = 100;
        }
    }

    public void sakit(String namaPenyakit) {
        if (!(listObject.contains(this))){
            System.out.println(nama + "  telah tiada");
            return;
        }
        int panjangPenyakit = namaPenyakit.length();
        kebahagiaan -= panjangPenyakit;
        if(kebahagiaan < 0){
            kebahagiaan = 0;
        }
        System.out.println(nama + " terkena penyakit " + namaPenyakit + " :0");
    }

    public String toString() {
        return "Nama\t\t: " + nama + "\nUmur\t\t: " + umur + "\nUang\t\t: " + uang + "\nKebahagiaan\t: " + kebahagiaan;
    }

    public void meninggal(){
        if (!(listObject.contains(this))) {
            System.out.println(nama + "  telah tiada");
            return;
        }
        System.out.println(nama + " meninggal dengan tenang, kebahagiaan : " + kebahagiaan);
        nama = "Almarhum " + nama;
        listObject.remove(this);
        if ((listObject.indexOf(this)) == (listObject.size() - 1)){
            uang = 0;
             System.out.println("Semua harta " + nama + " hangus");
        }
        else {
            Manusia penerima = listObject.get(listObject.size() - 1);
            penerima.setUang(penerima.getUang() + uang);
            uang = 0;
            System.out.println("Semua harta " + nama + " disumbangkan untuk " + penerima.getNama());
        }
    }
}
