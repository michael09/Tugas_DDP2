package lab9.event;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
* A class representing an event and its properties
*/
public class Event implements Comparable<Event>
{
    /** Name of event */
    private String name;
    /** Start time of the event */
    private LocalDateTime startTime;
    /** end time of the event */
    private LocalDateTime endTime;
    /** cost of the evnt */
    private BigInteger cost;

    /**
     * Constructor of the event
     * Initializes an event object
     * @param name Name of the event
     * @param startTime an LocalDateTime object of the event start time
     * @param endTime an LocalDateTime object of the event endTime
     * @param cost a cost of the event in BigInteger
     */
    public Event(String name, LocalDateTime startTime, LocalDateTime endTime, BigInteger cost) {
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.cost = cost;
    }

    /**
     * Accessor for name field.
     * @return name of this event instance
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Override method toString
     * @return String result about info of the event
     */
    @Override
    public String toString() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy, HH:mm:ss");
        return name + "\nWaktu mulai: " + startTime.format(format)+ "\nWaktu selesai: " + endTime.format(format)
                + "\nBiaya kehadiran: " + cost;
    }

    /**
     * method o check wheter the added event is overlapping with other registered event
     * @param event object of the event
     * @return boolean is it overlap or not
     */
    public boolean cekNoOverlap(Event event) {
        return this.endTime.isBefore(event.startTime) || event.endTime.isBefore(this.startTime);
    }

    /**
     * Accessor for cost of the event
     * @return BigInteger cost of the event
     */
    public BigInteger getCost() {
        return cost;
    }

    /**
     * method that implement interface comparable
     * @param o object of the event
     * @return int result
     */
    @Override
    public int compareTo(Event o) {
        return (int)(o.startTime.until(this.startTime, ChronoUnit.SECONDS));
    }
}
