package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
* Class representing event managing system
*/
public class EventSystem
{
    /**
    * List of events
    */
    private ArrayList<Event> events;
    
    /**
    * List of users
    */
    private ArrayList<User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * Add event to the system
     * @param name name of the event
     * @param startTimeStr String start time of the event
     * @param endTimeStr String end time of the event
     * @param costPerHourStr costPerHour of the event
     * @return String result of the method
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr)
    {
        if (findEvent(name) == null) {
            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");
            LocalDateTime waktuMulai = LocalDateTime.parse(startTimeStr, format);
            LocalDateTime waktuSelesai = LocalDateTime.parse(endTimeStr, format);
            if (waktuSelesai.isAfter(waktuMulai)) {
                events.add(new Event(name, waktuMulai, waktuSelesai, new BigInteger(costPerHourStr)));
                return "Event " + name + " berhasil ditambahkan!";
            }
            return "Waktu yang diinputkan tidak valid!";
        }
        return "Event " + name + " sudah ada!";
    }

    /**
     * add user to the system
     * @param name name of the user
     * @return String result of the method
     */
    public String addUser(String name)
    {
        if (findUser(name) == null) {
            users.add(new User(name));
            return "User " + name + " berhasil ditambahkan!";
        }
        return "User " + name + " sudah ada!";
    }

    /**
     * Method to register a user to an event
     * @param userName name of the user
     * @param eventName name of the event
     * @return String result of the method
     */
    public String registerToEvent(String userName, String eventName)
    {
        User pengguna = findUser(userName);
        Event acara = findEvent(eventName);
        if (pengguna == null && acara == null) {
            return "Tidak ada pengguna dengan nama " + userName
                    + " dan acara dengan nama " + eventName + "!";
        } else if (acara == null) {
            return "Tidak ada acara dengan nama " + eventName + "!";
        } else if (pengguna == null) {
            return "Tidak ada pengguna dengan nama " + userName + "!";
        } else if (pengguna.addEvent(acara)) {
            return userName + " berencana menghadiri " + eventName + "!";
        }
        return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
    }

    /**
     * method to find object user in the system
     * @param nama name of the user
     * @return Object User or null if nothing found
     */
    private User findUser(String nama) {
        for (User user: users) {
            if (user.getName().equals(nama)) {
                return user;
            }
        }
        return null;
    }

    /**
     * method to find object event in the system
     * @param event name of the event
     * @return Object Event or null if nothing found
     */
    private Event findEvent(String event) {
        for (Event acara: events) {
            if (acara.getName().equals(event)) {
                return acara;
            }
        }
        return null;
    }

    /**
     * method to call method toString in the Event object
     * @param event name of the event
     * @return String result of the event
     */
    public String getEvent(String event) {
        Event acara = findEvent(event);
        if (acara != null) {
            return acara.toString();
        }
        return "Tidak ada acara dengan nama " + event;
    }

    /**
     * method to return a n object User
     * @param name name of the user
     * @return Object User
     */
    public User getUser(String name) {
        return findUser(name);
    }
}