import java.util.Scanner;

public class RabbitHouse {
    public static void main (String[] args) {
        Scanner input = new Scanner(System.in);

        String tipe = input.next();
        String nama = input.next();
        String hasil;
        switch (tipe) {
            case "normal":
                hasil = String.valueOf(Normal((nama.length()), 1));
                System.out.println(hasil);
                break;
            case "palindrom":
                Palindrome(nama);
                System.out.println(sum);
                break;
            default:
                System.out.println("Maaf tidak ada tipe " + tipe);
                break;
        }
    }

    public  static int Normal(int panjangNama, int jumlahKelinci) {
        if (panjangNama == 1 ) {
            return 1;
        }
        else {
            return panjangNama * jumlahKelinci + Normal((panjangNama - 1), panjangNama * jumlahKelinci);
        }
    }
    static int sum = 0;
    public static void Palindrome(String nama) {
        int panjangNama = nama.length();
        if (CheckPalindrome(nama)) {
            return ;
        } else {
            sum += 1;
            for (int a = 0; a < panjangNama ; a++) {
                if (a == 0) {
                    Palindrome((nama.substring(1, panjangNama)));
                } else if (a == (panjangNama - 1)) {
                    Palindrome((nama.substring(0, panjangNama - 1)));
                } else {
                    Palindrome((nama.substring(0, a) + nama.substring(a + 1, panjangNama)));
                }
            }
        }
    }


    public static boolean CheckPalindrome(String nama) {
        int awal = 0;
        int akhir = nama.length() - 1;
        while (akhir > awal) {
            if (nama.charAt(awal) != nama.charAt(akhir)) {
                return false;
            }
            ++awal;
            --akhir;
        }
        return true;
    }
}