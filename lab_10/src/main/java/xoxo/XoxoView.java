package xoxo;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoView extends JFrame {
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    /**
     *
     */
    private JFrame mainPanel;

    /**
     *
     */
    private JLabel labelMessage, labelseed, labelKey;

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }


    /**
     * Constructs the GUI.
     */
    private void initGui() {
        mainPanel = new JFrame("Xoxo Encryptor/Decryptor");
        decryptButton = new JButton("Decrypt");
        encryptButton = new JButton("Encrypt");
        messageField = new JTextField();
        keyField = new JTextField();
        seedField = new JTextField();
        logField = new JTextArea();
        labelMessage = new JLabel("Message to encrypt/decrypt");
        labelKey = new JLabel("Kiss key/Hug key");
        labelseed = new JLabel("Seed");
        mainPanel.setLayout(new BorderLayout());
        JPanel panel0 = new JPanel(new GridLayout(0,1));
        JPanel panel1 = new JPanel(new BorderLayout());
        panel1.add(logField, BorderLayout.CENTER);
        JPanel panel3 = new JPanel(new GridLayout(1,0));
        panel3.add(decryptButton, BorderLayout.PAGE_END);
        panel3.add(encryptButton, BorderLayout.PAGE_END);
        panel1.setPreferredSize(new Dimension(400, 800));
        panel1.add(panel3, BorderLayout.PAGE_END);
        mainPanel.add(panel1, BorderLayout.EAST);
        panel0.add(labelMessage);
        panel0.add(messageField);
        panel0.add(labelKey);
        panel0.add(keyField);
        panel0.add(labelseed);
        panel0.add(seedField);
        mainPanel.add(panel0, BorderLayout.CENTER);
        mainPanel.setPreferredSize(new Dimension(1000, 800));
        mainPanel.pack();
        mainPanel.setVisible(true);
        mainPanel.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }

    public void setWarning(String warn) {
        JOptionPane.showMessageDialog(this, warn);
    }
}