package xoxo.crypto;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) {
        final int length = encryptedMessage.length();
        StringBuilder decryptedMessage = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int m = this.hugKeyString.charAt(i % this.hugKeyString.length());
            int hasil1 = m ^ seed;
            int hasil2 = hasil1 - 'a';
            int hasil3 = encryptedMessage.charAt(i) ^ hasil2;
            decryptedMessage.append((char) hasil3);
        }
        return decryptedMessage.toString();
    }
}