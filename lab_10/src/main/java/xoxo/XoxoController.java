package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.util.XoxoMessage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;
    private static int countFileEnc = 0;
    private static int countFileTxt = 0;

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        //TODO: Write your code for logic and everything here
        gui.setEncryptFunction(e -> encrypt());
        gui.setDecryptFunction(e -> decrypt());
    }

    //TODO: Create any methods that you want
    private void encrypt() {
        String kissKey = gui.getKeyText();
        String message = gui.getMessageText();
        String seed = gui.getSeedText();
        XoxoEncryption encrypt = null;
        try {
            encrypt = new XoxoEncryption(kissKey);
        } catch (KeyTooLongException e) {
            gui.setWarning("Key length must not exceed 28");
        } catch (InvalidCharacterException e) {
            gui.setWarning("Key can contain only A-Z, a-z, and @");
        }
        XoxoMessage hasil;
        try {
            if (seed.equals("")) {
                hasil = encrypt.encrypt(message);
                seed = "DEFAULT SEED";
            } else {
                hasil = encrypt.encrypt(message, Integer.parseInt(seed));
            }
        } catch (SizeTooBigException e) {
            gui.setWarning("Message size more than 10 Kbit");
            return;
        } catch (RangeExceededException e) {
            gui.setWarning("Seed must between 0 and 36 (inclusive)");
            return;
        } catch (NumberFormatException e) {
            gui.setWarning("Seed must be number");
            return;
        }
        fileEnc(hasil, seed);
    }

    private void fileEnc(XoxoMessage hasil, String seed) {
        File file = new File("E:\\Documents\\KELAS UI\\DDP2\\Git upload\\lab_10\\src\\main\\java\\ResultEncrypt" + countFileEnc + ".enc");
        boolean created = false;
        try {
            created = file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(hasil.getEncryptedMessage());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            gui.appendLog("Cannot create file, encrypt failed");
        }
        if (created) {
            gui.appendLog("Encrypted: " + hasil.getEncryptedMessage());
            gui.appendLog("Hug key: " + hasil.getHugKey().getKeyString());
            gui.appendLog("Seed: " + seed);
            countFileEnc += 1;
        } else {
            gui.appendLog("Failed to encrypt");
        }
    }

    private void decrypt() {
        String hugKey = gui.getKeyText();
        XoxoDecryption decrypt = new XoxoDecryption(hugKey);
        int seed;
        if (gui.getSeedText().equals("")) {
            seed = 18;
        } else {
            seed = Integer.parseInt(gui.getSeedText());
        }
        String encryptedMessage = gui.getMessageText();
        String hasil = decrypt.decrypt(encryptedMessage, seed);
        fileTxt(hasil);
    }

    private void fileTxt(String hasil) {
        File file = new File("E:\\Documents\\KELAS UI\\DDP2\\Git upload\\lab_10\\src\\main\\java\\ResultDecrypt" + countFileTxt + ".txt");
        boolean created = false;
        try {
            created = file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(hasil);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            gui.appendLog("Cannot create file, decrypt failed");
        }
        if (created) {
            gui.appendLog("Decryption result: " + hasil);
            countFileTxt += 1;
        } else {
            gui.appendLog("Failed to decrypt");
        }
    }
}